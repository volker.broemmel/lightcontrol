# lightcontrol
nodejs project that lets you create rules in a ~~mysql/mariadb~~ json config file and gives you control over a PCF8591 DAC via I2C to control dimmable light tubes. Roughly speaking. :-)

Preferably using a RaspberryPI or similar.

This is still under heavy development, but all technical details and hardware was already tested and is working.

Feel free to contact me for details.


Installation steps:
* install nodejs
* activate ssh
* activate i2c
* ~~install mysql~~
* ~~install apache (used only for phpmyadmin mysql administration tool)~~


# GPIO settings in /boot/config.txt (RaspberryPI / Raspbian)
see https://www.raspberrypi.com/documentation/computers/config_txt.html#gpio-control

```
[gpio]
    gpio=14-15,18=ip,dl,pu
    gpio=4,17=op,dh,pd
```