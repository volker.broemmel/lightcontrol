module.exports = {
  GetCurrentTime: function () {
    return getCurrentDateObject().toLocaleTimeString()
  },
  GetCurrentDate: function () {
    return getCurrentDateObject().toLocaleDateString()
  },
  GetCurrentDateTime: function () {
    return getCurrentDateObject().toLocaleString()
  },
  GetCurrentDateObject: () => {
    return getCurrentDateObject()
  }
}

function getCurrentDateObject(){
  return new Date(Date.now())
}
