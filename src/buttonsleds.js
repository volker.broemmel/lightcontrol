'use strict';

/**
 * initialize hardware relevant stuff
 */
let Gpio
let Relais1
let Relais2
let ButtonAutomatic
let Button100Percent
let Button0Percent

try {
  Gpio = require('onoff').Gpio

  Relais1 = new Gpio(4, 'out')
  Relais2 = new Gpio(17, 'out')

  Button0Percent = new Gpio(14, 'in', 'falling', { debounceTimeout: 20 })
  ButtonAutomatic = new Gpio(15, 'in', 'falling', { debounceTimeout: 20 })
  Button100Percent = new Gpio(18, 'in', 'falling', { debounceTimeout: 20 })

  Relais1.writeSync(1)
  Relais2.writeSync(1)

  ButtonAutomatic.watch((err, value) => {
    if (err) { console.error(err.message + ' / ' + value) }
    console.log('ButtonAutomatic changed. Value: ' + value) 
  })

  Button100Percent.watch((err, value) => {
    if (err) { console.error(err.message + ' / ' + value) }
    console.log('Button100Percent changed. Value: ' + value) 
  })

  Button0Percent.watch((err, value) => {
    if (err) { console.error(err.message + ' / ' + value) }
    console.log('Button0Percent changed. Value: ' + value)
  })

  console.log('Created and initialized \'onoff\' Relais1=GPIO4, Relais2=GPIO17')
} catch (e) {
  console.error('Could not create \'onoff\'...')
}
 
console.log('ButtonAutomatic.activeLow : %s', ButtonAutomatic.activeLow())
console.log('Button100Percent.activeLow: %s', Button100Percent.activeLow())
console.log('Button0Percent.activeLow  : %s', Button0Percent.activeLow())


console.log('ButtonAutomatic : %s', ButtonAutomatic.readSync())
console.log('Button100Percent: %s', Button100Percent.readSync())
console.log('Button0Percent  : %s', Button0Percent.readSync())



// var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO

// var LEDAuto  = new Gpio(13, 'out'); 
// var LED100  = new Gpio(19, 'out'); 
// var LED50  = new Gpio(26, 'out'); 

// var btnAuto = new Gpio(4, 'in', 'both'); //use GPIO as input, and 'both' button presses, and releases should be handled
// var btn100 = new Gpio(17, 'in', 'both'); //use GPIO as input, and 'both' button presses, and releases should be handled
// var btn50 = new Gpio(27, 'in', 'both'); //use GPIO as input, and 'both' button presses, and releases should be handled

// btnAuto.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
//   if (err) { //if an error
//     console.error('There was an error', err); //output error message to console
//   return;
//   }
//   LEDAuto.writeSync(value); //turn LED on or off depending on the button state (0 or 1)
// });

// btn100.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
//     if (err) { //if an error
//       console.error('There was an error', err); //output error message to console
//     return;
//     }
//     LED100.writeSync(value); //turn LED on or off depending on the button state (0 or 1)
//   });

//   btn50.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
//     if (err) { //if an error
//       console.error('There was an error', err); //output error message to console
//     return;
//     }
//     btn50.writeSync(value); //turn LED on or off depending on the button state (0 or 1)
//   });
  
// function unexportOnClose() { //function to run when exiting program
//     LEDAuto.writeSync(0); // Turn LED off
//     LEDAuto.unexport(); // Unexport LED GPIO to free resources
//     btnAuto.unexport(); // Unexport Button GPIO to free resources
//     LED100.writeSync(0); // Turn LED off
//     LED100.unexport(); // Unexport LED GPIO to free resources
//     btn100.unexport(); // Unexport Button GPIO to free resources
//     btn50.writeSync(0); // Turn LED off
//     btn50.unexport(); // Unexport LED GPIO to free resources
//     btn50.unexport(); // Unexport Button GPIO to free resources
// };

process.on('SIGINT', () => {
  Relais1.unexport()
  Relais2.unexport()
  ButtonAutomatic.unexport()
  Button100Percent.unexport()
  Button0Percent.unexport()

  process.exit()
}); //function to run when user closes using ctrl+c 